from scanner import ConScanner


class SharedObject:
    scanner: ConScanner = None


def get_scanner() -> ConScanner:
    return SharedObject.scanner
