import os
import pathlib

import yaml


PROJECT_DIR = pathlib.Path(__file__).parent     # 应用程序目录
CFG_FILE = PROJECT_DIR / 'config' / 'settings.yaml'

if not os.path.isfile(CFG_FILE):
    raise FileNotFoundError(CFG_FILE)
configs = yaml.safe_load(open(CFG_FILE, 'r', encoding='utf-8'))   # 应用程序配置


WORK_DIR = pathlib.Path(configs['workdir']) if configs.get('workdir') else PROJECT_DIR   # 工作目录
LOGS_DIR = WORK_DIR / 'logs'     # 日志文件目录
os.makedirs(LOGS_DIR, exist_ok=True)

# log配置字典
LOGGING_DICT = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        # 简要日志格式
        'brief': {
            'format': '%(message)s'
        },
        # 默认日志格式
        'default': {
            'format': '[%(asctime)s] %(levelname)-8s %(name)-15s %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
        # 自定义格式
        'custom': {

        },
    },
    'filters': {},
    'handlers': {
        # 输出到控制台
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',  # 打印到屏幕
            'formatter': 'brief'
        },
        # 输入到日志文件
        'info': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',  # 保存到文件
            'formatter': 'default',
            'filename': LOGS_DIR / 'log.txt',       # 日志文件路径
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 5,
            'encoding': 'utf-8'
        },
        # 错误日志（包含警告）
        'error': {
                    'level': 'WARNING',
                    'class': 'logging.handlers.RotatingFileHandler',  # 保存到文件
                    'formatter': 'default',
                    'filename': LOGS_DIR / 'error.txt',       # 日志文件路径
                    'maxBytes': 1024 * 1024 * 5,
                    'backupCount': 3,
                    'encoding': 'utf-8'
        },
        # 审计日志
        'audit': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',  # 按天保存到文件
            'formatter': 'default',
            'filename': LOGS_DIR / 'audit_log.txt',  # 日志文件路径
            'when': 'd',
            'interval': 1,
            'backupCount': 30,
            'encoding': 'utf-8'
        },
    },
    'loggers': {
        # 默认的logger应用如下配置
        '': {
            'handlers': ['console', 'info', 'error'],
            'level': 'INFO',
            'propagate': False,  # 向上（更高level的logger）传递
        },
        'lame': {
            'handlers': ['console', 'info', 'error'],
            'level': 'INFO',
            'propagate': False,  # 向上（更高level的logger）传递
        },
        'audit': {
            'handlers': ['audit', 'console'],
            'level': 'INFO',
            'propagate': False,  # 向上（更高level的logger）传递
        },
    },
}
