import time
from concurrent.futures import ThreadPoolExecutor

from scanner import ConScanner

scanner = ConScanner(r'D:\Work\Data\lame', scan_params='-cav=1 -product=rising -rdm.vba=1 -precise-format=0')
scanner.run()

start = time.time()
with ThreadPoolExecutor(max_workers=1) as pool:
    arr = []
    for i in range(100):
        arr.append(pool.submit(scanner.scan_file, r"E:\Work\Data\temp\YDWebShell_Samples\Vbs\WebshellGen\4F44B090CF6C578EE66D265E5FEDE051.bin"))
    for f in arr:
        print(f.result())
print(time.time() - start)
scanner.close()
