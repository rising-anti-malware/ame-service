FROM python:3.8.2
LABEL maintainer="chenqi <chenqi@rising.com.cn>" app=rising-lame-service

RUN cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo 'Asia/Shanghai' >/etc/timezone

ENV ROOTDIR="/var/local/rising"
ENV WORKERS=3

RUN mkdir -p ${ROOTDIR}
COPY ./lame-service ${ROOTDIR}/lame-service
COPY ./engine ${ROOTDIR}/engine

WORKDIR ${ROOTDIR}/lame-service
RUN pip install -r requirements.txt -i https://pypi.douban.com/simple
EXPOSE 9977
#ENTRYPOINT python app.py --host 0.0.0.0 --workers $WORKERS
ENTRYPOINT gunicorn -b 0.0.0.0:9977 -w $WORKERS -k uvicorn.workers.UvicornWorker app:app