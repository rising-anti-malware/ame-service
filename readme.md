# 概述
该项目通过瑞星引擎SDK对外提供在线查毒服务，当前属于Demo阶段
# 启动方法
## 本机启动
*需要* **python3.7+** *环境*
- git clone https://gitee.com/rising-anti-malware/ame-service.git
- cd ame-service/lame-service
- pip install -r requirements.txt
- vi config/settings.yaml  **设置SDK引擎目录及工作目录，启动前先确保引擎、病毒库、授权文件可用**
- python app.py --host 0.0.0.0 --port 9977 --workers 3
>
*配置项说明*
- **lamedir:** SDK引擎目录，该目录中放置瑞星的引擎、病毒库和授权文件，*需要联系瑞星获取*
- **scan_params:** SDK引擎启动时设置的扫描参数，*需要联系瑞星获取*
- **lib_name:** 病毒库名称，用于在线升级病毒库，*需要联系瑞星获取*
- **workdir:** 工作目录，主要用于存放日志及临时文件，*确保目录可读可写*
- **max_file_size:** 设置在线扫描样本文件的大小上限
## Docker启动
#### 制作Docker镜像
- git clone https://gitee.com/rising-anti-malware/ame-service.git
- cd ame-service
- vi config/settings.yaml  
    - **设置lamedir:** */var/local/rising/engine*
    - **设置workdir:** */var/local/rising/data*
    - *其他配置参考配置项说明*
- docker build -t lame-service .  **build之前先确保当前目录下有引擎存在，*cd ./engine***
#### 启动Docker容器
docker run -itd -p 0.0.0.0:9977:9977 -e WORKERS=2 -v /var/local/rising/data:/var/local/rising/data lame-service
- **环境变量*WORKERS*可以配置启动的Worker数量**
- **Docker内部数据目录为：*/var/local/rising/data*，可以配置宿主机目录用于映射，方便数据持久化**
## 查看接口文档
启动成功后，可以通过下面的链接查看接口使用文档，端口与第2步中映射的端口一致
>
    http://宿主机IP:9977/docs
