1、获取Docker镜像
    docker pull risingdk/lame-service:latest
2、启动Docker容器
    docker run -itd -p 0.0.0.0:19977:9977 -e WORKERS=2 -v /var/local/rising/data:/var/local/rising/data risingdk/lame-service:latest
    环境变量WORKERS可以配置启动的Worker数量，默认为3个
    Docker内部数据目录为/var/local/rising/data，可以配置宿主机目录用于映射，方便数据持久化
    Docker内部查毒服务端口为9977，可以映射宿主机端口到该端口
3、测试在线查毒接口
    容器启动成功后，可以通过http://宿主机IP:19977/docs  查看接口使用文档，端口与第2步中映射的端口一致